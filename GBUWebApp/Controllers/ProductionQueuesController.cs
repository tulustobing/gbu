﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GBUWebApp.Models;

namespace GBUWebApp.Controllers
{
    public class ProductionQueuesController : Controller
    {
        private gbuEntities db = new gbuEntities();

        // GET: ProductionQueues
        public ActionResult Index()
        {
            var productionQueues = db.ProductionQueues.Include(p => p.Machine).Include(p => p.WorkOrder);
            return View(productionQueues.ToList());
        }

        // GET: ProductionQueues/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductionQueue productionQueue = db.ProductionQueues.Find(id);
            if (productionQueue == null)
            {
                return HttpNotFound();
            }
            return View(productionQueue);
        }

        // GET: ProductionQueues/Create
        public ActionResult Create()
        {
            ViewBag.MachineId = new SelectList(db.Machines, "Id", "Name");
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId");
            return View();
        }

        // POST: ProductionQueues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MachineId,WorkOrderId,Priority,Status,EstimationStart,StartDate,EstimationEnd,EndDate")] ProductionQueue productionQueue)
        {
            if (ModelState.IsValid)
            {
                db.ProductionQueues.Add(productionQueue);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MachineId = new SelectList(db.Machines, "Id", "Name", productionQueue.MachineId);
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", productionQueue.WorkOrderId);
            return View(productionQueue);
        }

        // GET: ProductionQueues/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductionQueue productionQueue = db.ProductionQueues.Find(id);
            if (productionQueue == null)
            {
                return HttpNotFound();
            }
            ViewBag.MachineId = new SelectList(db.Machines, "Id", "Name", productionQueue.MachineId);
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", productionQueue.WorkOrderId);
            return View(productionQueue);
        }

        // POST: ProductionQueues/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MachineId,WorkOrderId,Priority,Status,EstimationStart,StartDate,EstimationEnd,EndDate")] ProductionQueue productionQueue)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productionQueue).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MachineId = new SelectList(db.Machines, "Id", "Name", productionQueue.MachineId);
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", productionQueue.WorkOrderId);
            return View(productionQueue);
        }

        // GET: ProductionQueues/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductionQueue productionQueue = db.ProductionQueues.Find(id);
            if (productionQueue == null)
            {
                return HttpNotFound();
            }
            return View(productionQueue);
        }

        // POST: ProductionQueues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductionQueue productionQueue = db.ProductionQueues.Find(id);
            db.ProductionQueues.Remove(productionQueue);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
