﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GBUWebApp.Models;

namespace GBUWebApp.Controllers
{
    public class DefectDeliveriesController : Controller
    {
        private gbuEntities db = new gbuEntities();

        // GET: DefectDeliveries
        public ActionResult Index()
        {
            var defectDeliveries = db.DefectDeliveries.Include(d => d.IntermediateGood);
            return View(defectDeliveries.ToList());
        }

        // GET: DefectDeliveries/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DefectDelivery defectDelivery = db.DefectDeliveries.Find(id);
            if (defectDelivery == null)
            {
                return HttpNotFound();
            }
            return View(defectDelivery);
        }

        // GET: DefectDeliveries/Create
        public ActionResult Create()
        {
            ViewBag.IntermediateId = new SelectList(db.IntermediateGoods, "Id", "Name");
            return View();
        }

        // POST: DefectDeliveries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IntermediateId,Amount,Color,Status,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] DefectDelivery defectDelivery)
        {
            if (ModelState.IsValid)
            {
                db.DefectDeliveries.Add(defectDelivery);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IntermediateId = new SelectList(db.IntermediateGoods, "Id", "Name", defectDelivery.IntermediateId);
            return View(defectDelivery);
        }

        // GET: DefectDeliveries/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DefectDelivery defectDelivery = db.DefectDeliveries.Find(id);
            if (defectDelivery == null)
            {
                return HttpNotFound();
            }
            ViewBag.IntermediateId = new SelectList(db.IntermediateGoods, "Id", "Name", defectDelivery.IntermediateId);
            return View(defectDelivery);
        }

        // POST: DefectDeliveries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IntermediateId,Amount,Color,Status,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] DefectDelivery defectDelivery)
        {
            if (ModelState.IsValid)
            {
                db.Entry(defectDelivery).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IntermediateId = new SelectList(db.IntermediateGoods, "Id", "Name", defectDelivery.IntermediateId);
            return View(defectDelivery);
        }

        // GET: DefectDeliveries/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DefectDelivery defectDelivery = db.DefectDeliveries.Find(id);
            if (defectDelivery == null)
            {
                return HttpNotFound();
            }
            return View(defectDelivery);
        }

        // POST: DefectDeliveries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            DefectDelivery defectDelivery = db.DefectDeliveries.Find(id);
            db.DefectDeliveries.Remove(defectDelivery);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
