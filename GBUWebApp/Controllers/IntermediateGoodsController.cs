﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GBUWebApp.Models;

namespace GBUWebApp.Controllers
{
    public class IntermediateGoodsController : Controller
    {
        private gbuEntities db = new gbuEntities();

        // GET: IntermediateGoods
        public ActionResult Index()
        {
            return View(db.IntermediateGoods.ToList());
        }

        // GET: IntermediateGoods/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IntermediateGood intermediateGood = db.IntermediateGoods.Find(id);
            if (intermediateGood == null)
            {
                return HttpNotFound();
            }
            return View(intermediateGood);
        }

        // GET: IntermediateGoods/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IntermediateGoods/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Weight,Color,CyclicTime,SellingPrice,Amount,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] IntermediateGood intermediateGood)
        {
            if (ModelState.IsValid)
            {
                db.IntermediateGoods.Add(intermediateGood);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(intermediateGood);
        }

        // GET: IntermediateGoods/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IntermediateGood intermediateGood = db.IntermediateGoods.Find(id);
            if (intermediateGood == null)
            {
                return HttpNotFound();
            }
            return View(intermediateGood);
        }

        // POST: IntermediateGoods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Weight,Color,CyclicTime,SellingPrice,Amount,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] IntermediateGood intermediateGood)
        {
            if (ModelState.IsValid)
            {
                db.Entry(intermediateGood).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(intermediateGood);
        }

        // GET: IntermediateGoods/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IntermediateGood intermediateGood = db.IntermediateGoods.Find(id);
            if (intermediateGood == null)
            {
                return HttpNotFound();
            }
            return View(intermediateGood);
        }

        // POST: IntermediateGoods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            IntermediateGood intermediateGood = db.IntermediateGoods.Find(id);
            db.IntermediateGoods.Remove(intermediateGood);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
