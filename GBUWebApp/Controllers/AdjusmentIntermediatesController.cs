﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GBUWebApp.Models;

namespace GBUWebApp.Controllers
{
    public class AdjusmentIntermediatesController : Controller
    {
        private gbuEntities db = new gbuEntities();

        // GET: AdjusmentIntermediates
        public ActionResult Index()
        {
            var adjusmentIntermediates = db.AdjusmentIntermediates.Include(a => a.IntermediateGood);
            return View(adjusmentIntermediates.ToList());
        }

        // GET: AdjusmentIntermediates/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdjusmentIntermediate adjusmentIntermediate = db.AdjusmentIntermediates.Find(id);
            if (adjusmentIntermediate == null)
            {
                return HttpNotFound();
            }
            return View(adjusmentIntermediate);
        }

        // GET: AdjusmentIntermediates/Create
        public ActionResult Create()
        {
            ViewBag.IntermediateId = new SelectList(db.IntermediateGoods, "Id", "Name");
            return View();
        }

        // POST: AdjusmentIntermediates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IntermediateId,Amount,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] AdjusmentIntermediate adjusmentIntermediate)
        {
            if (ModelState.IsValid)
            {
                db.AdjusmentIntermediates.Add(adjusmentIntermediate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IntermediateId = new SelectList(db.IntermediateGoods, "Id", "Name", adjusmentIntermediate.IntermediateId);
            return View(adjusmentIntermediate);
        }

        // GET: AdjusmentIntermediates/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdjusmentIntermediate adjusmentIntermediate = db.AdjusmentIntermediates.Find(id);
            if (adjusmentIntermediate == null)
            {
                return HttpNotFound();
            }
            ViewBag.IntermediateId = new SelectList(db.IntermediateGoods, "Id", "Name", adjusmentIntermediate.IntermediateId);
            return View(adjusmentIntermediate);
        }

        // POST: AdjusmentIntermediates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IntermediateId,Amount,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] AdjusmentIntermediate adjusmentIntermediate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(adjusmentIntermediate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IntermediateId = new SelectList(db.IntermediateGoods, "Id", "Name", adjusmentIntermediate.IntermediateId);
            return View(adjusmentIntermediate);
        }

        // GET: AdjusmentIntermediates/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdjusmentIntermediate adjusmentIntermediate = db.AdjusmentIntermediates.Find(id);
            if (adjusmentIntermediate == null)
            {
                return HttpNotFound();
            }
            return View(adjusmentIntermediate);
        }

        // POST: AdjusmentIntermediates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            AdjusmentIntermediate adjusmentIntermediate = db.AdjusmentIntermediates.Find(id);
            db.AdjusmentIntermediates.Remove(adjusmentIntermediate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
