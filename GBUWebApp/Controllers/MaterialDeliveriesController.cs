﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GBUWebApp.Models;

namespace GBUWebApp.Controllers
{
    public class MaterialDeliveriesController : Controller
    {
        private gbuEntities db = new gbuEntities();

        // GET: MaterialDeliveries
        public ActionResult Index()
        {
            var materialDeliveries = db.MaterialDeliveries.Include(m => m.WorkOrder);
            return View(materialDeliveries.ToList());
        }

        // GET: MaterialDeliveries/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaterialDelivery materialDelivery = db.MaterialDeliveries.Find(id);
            if (materialDelivery == null)
            {
                return HttpNotFound();
            }
            return View(materialDelivery);
        }

        // GET: MaterialDeliveries/Create
        public ActionResult Create()
        {
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId");
            return View();
        }

        // POST: MaterialDeliveries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,WorkOrderId,Amount,Status,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] MaterialDelivery materialDelivery)
        {
            if (ModelState.IsValid)
            {
                db.MaterialDeliveries.Add(materialDelivery);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", materialDelivery.WorkOrderId);
            return View(materialDelivery);
        }

        // GET: MaterialDeliveries/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaterialDelivery materialDelivery = db.MaterialDeliveries.Find(id);
            if (materialDelivery == null)
            {
                return HttpNotFound();
            }
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", materialDelivery.WorkOrderId);
            return View(materialDelivery);
        }

        // POST: MaterialDeliveries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,WorkOrderId,Amount,Status,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] MaterialDelivery materialDelivery)
        {
            if (ModelState.IsValid)
            {
                db.Entry(materialDelivery).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", materialDelivery.WorkOrderId);
            return View(materialDelivery);
        }

        // GET: MaterialDeliveries/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaterialDelivery materialDelivery = db.MaterialDeliveries.Find(id);
            if (materialDelivery == null)
            {
                return HttpNotFound();
            }
            return View(materialDelivery);
        }

        // POST: MaterialDeliveries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            MaterialDelivery materialDelivery = db.MaterialDeliveries.Find(id);
            db.MaterialDeliveries.Remove(materialDelivery);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: MaterialDeliveries/Details/5
        public ActionResult CreateNewMaterialDelivery(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            MaterialDelivery materialDelivery = new MaterialDelivery();
            materialDelivery.WorkOrderId = id;
            if (materialDelivery == null)
            {
                return HttpNotFound();
            }
            return View(materialDelivery);
        }

        // POST: MaterialDeliveries/CreateNewMaterialDelivery/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNewMaterialDelivery(MaterialDelivery materialDelivery)
        {
            if (ModelState.IsValid)
            {
                materialDelivery.WorkOrderId = materialDelivery.Id;
                materialDelivery.Id = "BBDEL-" + materialDelivery.Id.Substring(4);
                materialDelivery.Status = "ACCEPTED";
                materialDelivery.CreatedBy = "Sadisx";
                materialDelivery.CreateDate = DateTime.Now;
                db.MaterialDeliveries.Add(materialDelivery);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", materialDelivery.WorkOrderId);
            return View(materialDelivery);
        }
    }
}
