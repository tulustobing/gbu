﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GBUWebApp.Models;

namespace GBUWebApp.Controllers
{
    public class AdjusmentFinishedGoodsController : Controller
    {
        private gbuEntities db = new gbuEntities();

        // GET: AdjusmentFinishedGoods
        public ActionResult Index()
        {
            return View(db.AdjusmentFinishedGoods.ToList());
        }

        // GET: AdjusmentFinishedGoods/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdjusmentFinishedGood adjusmentFinishedGood = db.AdjusmentFinishedGoods.Find(id);
            if (adjusmentFinishedGood == null)
            {
                return HttpNotFound();
            }
            return View(adjusmentFinishedGood);
        }

        // GET: AdjusmentFinishedGoods/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdjusmentFinishedGoods/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Amount,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] AdjusmentFinishedGood adjusmentFinishedGood)
        {
            if (ModelState.IsValid)
            {
                db.AdjusmentFinishedGoods.Add(adjusmentFinishedGood);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(adjusmentFinishedGood);
        }

        // GET: AdjusmentFinishedGoods/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdjusmentFinishedGood adjusmentFinishedGood = db.AdjusmentFinishedGoods.Find(id);
            if (adjusmentFinishedGood == null)
            {
                return HttpNotFound();
            }
            return View(adjusmentFinishedGood);
        }

        // POST: AdjusmentFinishedGoods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Amount,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] AdjusmentFinishedGood adjusmentFinishedGood)
        {
            if (ModelState.IsValid)
            {
                db.Entry(adjusmentFinishedGood).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(adjusmentFinishedGood);
        }

        // GET: AdjusmentFinishedGoods/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdjusmentFinishedGood adjusmentFinishedGood = db.AdjusmentFinishedGoods.Find(id);
            if (adjusmentFinishedGood == null)
            {
                return HttpNotFound();
            }
            return View(adjusmentFinishedGood);
        }

        // POST: AdjusmentFinishedGoods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            AdjusmentFinishedGood adjusmentFinishedGood = db.AdjusmentFinishedGoods.Find(id);
            db.AdjusmentFinishedGoods.Remove(adjusmentFinishedGood);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
