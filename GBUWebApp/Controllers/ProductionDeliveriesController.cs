﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GBUWebApp.Models;

namespace GBUWebApp.Controllers
{
    public class ProductionDeliveriesController : Controller
    {
        private gbuEntities db = new gbuEntities();

        // GET: ProductionDeliveries
        public ActionResult Index()
        {
            var productionDeliveries = db.ProductionDeliveries.Include(p => p.Machine).Include(p => p.WorkOrder);
            return View(productionDeliveries.ToList());
        }

        // GET: ProductionDeliveries/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductionDelivery productionDelivery = db.ProductionDeliveries.Find(id);
            if (productionDelivery == null)
            {
                return HttpNotFound();
            }
            return View(productionDelivery);
        }

        // GET: ProductionDeliveries/Create
        public ActionResult Create()
        {
            ViewBag.MachineId = new SelectList(db.Machines, "Id", "Name");
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId");
            return View();
        }

        // POST: ProductionDeliveries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,WorkOrderId,Amount,MachineId,MachineStatus,Status,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] ProductionDelivery productionDelivery)
        {
            if (ModelState.IsValid)
            {
                db.ProductionDeliveries.Add(productionDelivery);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MachineId = new SelectList(db.Machines, "Id", "Name", productionDelivery.MachineId);
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", productionDelivery.WorkOrderId);
            return View(productionDelivery);
        }

        // GET: ProductionDeliveries/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductionDelivery productionDelivery = db.ProductionDeliveries.Find(id);
            if (productionDelivery == null)
            {
                return HttpNotFound();
            }
            ViewBag.MachineId = new SelectList(db.Machines, "Id", "Name", productionDelivery.MachineId);
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", productionDelivery.WorkOrderId);
            return View(productionDelivery);
        }

        // POST: ProductionDeliveries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,WorkOrderId,Amount,MachineId,MachineStatus,Status,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] ProductionDelivery productionDelivery)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productionDelivery).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MachineId = new SelectList(db.Machines, "Id", "Name", productionDelivery.MachineId);
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", productionDelivery.WorkOrderId);
            return View(productionDelivery);
        }

        // GET: ProductionDeliveries/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductionDelivery productionDelivery = db.ProductionDeliveries.Find(id);
            if (productionDelivery == null)
            {
                return HttpNotFound();
            }
            return View(productionDelivery);
        }

        // POST: ProductionDeliveries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ProductionDelivery productionDelivery = db.ProductionDeliveries.Find(id);
            db.ProductionDeliveries.Remove(productionDelivery);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: MaterialDeliveries/Details/5
        public ActionResult CreateNewProductionDelivery(string WorkOrderId)
        {
            if (WorkOrderId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductionDelivery productionDelivery = new ProductionDelivery();
            productionDelivery.WorkOrderId = WorkOrderId;
            if (productionDelivery == null)
            {
                return HttpNotFound();
            }
            return View(productionDelivery);
        }

        // POST: MaterialDeliveries/CreateNewMaterialDelivery/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNewProductionDelivery(ProductionDelivery productionDelivery)
        {
            if (ModelState.IsValid)
            {
                //productionDelivery.WorkOrderId = productionDelivery.Id;
                productionDelivery.Id = "PRODDEL-" + productionDelivery.WorkOrderId.Substring(4);
                productionDelivery.Status = "ACCEPTED";
                productionDelivery.CreatedBy = "Bayi Sehat";
                productionDelivery.CreateDate = DateTime.Now;
                db.ProductionDeliveries.Add(productionDelivery);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", productionDelivery.WorkOrderId);
            return View(productionDelivery);
        }
    }
}
