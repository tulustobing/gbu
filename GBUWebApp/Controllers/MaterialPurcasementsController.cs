﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GBUWebApp.Models;

namespace GBUWebApp.Controllers
{
    public class MaterialPurcasementsController : Controller
    {
        private gbuEntities db = new gbuEntities();

        // GET: MaterialPurcasements
        public ActionResult Index()
        {
            var materialPurcasements = db.MaterialPurcasements.Include(m => m.Material);
            return View(materialPurcasements.ToList());
        }

        // GET: MaterialPurcasements/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaterialPurcasement materialPurcasement = db.MaterialPurcasements.Find(id);
            if (materialPurcasement == null)
            {
                return HttpNotFound();
            }
            return View(materialPurcasement);
        }

        // GET: MaterialPurcasements/Create
        public ActionResult Create()
        {
            ViewBag.MaterialId = new SelectList(db.Materials, "Id", "Name");
            return View();
        }

        // POST: MaterialPurcasements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MaterialId,Amount,Price,BuyingDate,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] MaterialPurcasement materialPurcasement)
        {
            if (ModelState.IsValid)
            {
                db.MaterialPurcasements.Add(materialPurcasement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MaterialId = new SelectList(db.Materials, "Id", "Name", materialPurcasement.MaterialId);
            return View(materialPurcasement);
        }

        // GET: MaterialPurcasements/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaterialPurcasement materialPurcasement = db.MaterialPurcasements.Find(id);
            if (materialPurcasement == null)
            {
                return HttpNotFound();
            }
            ViewBag.MaterialId = new SelectList(db.Materials, "Id", "Name", materialPurcasement.MaterialId);
            return View(materialPurcasement);
        }

        // POST: MaterialPurcasements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MaterialId,Amount,Price,BuyingDate,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] MaterialPurcasement materialPurcasement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(materialPurcasement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MaterialId = new SelectList(db.Materials, "Id", "Name", materialPurcasement.MaterialId);
            return View(materialPurcasement);
        }

        // GET: MaterialPurcasements/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaterialPurcasement materialPurcasement = db.MaterialPurcasements.Find(id);
            if (materialPurcasement == null)
            {
                return HttpNotFound();
            }
            return View(materialPurcasement);
        }

        // POST: MaterialPurcasements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            MaterialPurcasement materialPurcasement = db.MaterialPurcasements.Find(id);
            db.MaterialPurcasements.Remove(materialPurcasement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
