﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GBUWebApp.Models;

namespace GBUWebApp.Controllers
{
    public class IntermediateDeliveriesController : Controller
    {
        private gbuEntities db = new gbuEntities();

        // GET: IntermediateDeliveries
        public ActionResult Index()
        {
            var intermediateDeliveries = db.IntermediateDeliveries.Include(i => i.WorkOrder);
            return View(intermediateDeliveries.ToList());
        }

        // GET: IntermediateDeliveries/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IntermediateDelivery intermediateDelivery = db.IntermediateDeliveries.Find(id);
            if (intermediateDelivery == null)
            {
                return HttpNotFound();
            }
            return View(intermediateDelivery);
        }

        // GET: IntermediateDeliveries/Create
        public ActionResult Create()
        {
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId");
            return View();
        }

        // POST: IntermediateDeliveries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,WorkOrderId,Amount,Status,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] IntermediateDelivery intermediateDelivery)
        {
            if (ModelState.IsValid)
            {
                db.IntermediateDeliveries.Add(intermediateDelivery);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", intermediateDelivery.WorkOrderId);
            return View(intermediateDelivery);
        }

        // GET: IntermediateDeliveries/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IntermediateDelivery intermediateDelivery = db.IntermediateDeliveries.Find(id);
            if (intermediateDelivery == null)
            {
                return HttpNotFound();
            }
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", intermediateDelivery.WorkOrderId);
            return View(intermediateDelivery);
        }

        // POST: IntermediateDeliveries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,WorkOrderId,Amount,Status,Remarks,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] IntermediateDelivery intermediateDelivery)
        {
            if (ModelState.IsValid)
            {
                db.Entry(intermediateDelivery).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", intermediateDelivery.WorkOrderId);
            return View(intermediateDelivery);
        }

        // GET: IntermediateDeliveries/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IntermediateDelivery intermediateDelivery = db.IntermediateDeliveries.Find(id);
            if (intermediateDelivery == null)
            {
                return HttpNotFound();
            }
            return View(intermediateDelivery);
        }

        // POST: IntermediateDeliveries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            IntermediateDelivery intermediateDelivery = db.IntermediateDeliveries.Find(id);
            db.IntermediateDeliveries.Remove(intermediateDelivery);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: MaterialDeliveries/Details/5
        public ActionResult CreateNewIntermediateDelivery(string WorkOrderId)
        {
            if (WorkOrderId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IntermediateDelivery intermediateDelivery = new IntermediateDelivery();
            intermediateDelivery.WorkOrderId = WorkOrderId;
            if (intermediateDelivery == null)
            {
                return HttpNotFound();
            }
            return View(intermediateDelivery);
        }

        // POST: MaterialDeliveries/CreateNewMaterialDelivery/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNewIntermediateDelivery(IntermediateDelivery intermediateDelivery)
        {
            if (ModelState.IsValid)
            {
                intermediateDelivery.Id = "BBDEL-" + intermediateDelivery.WorkOrderId.Substring(4);
                intermediateDelivery.Status = "ACCEPTED";
                intermediateDelivery.CreatedBy = "Bayi Saburr";
                intermediateDelivery.CreateDate = DateTime.Now;
                db.IntermediateDeliveries.Add(intermediateDelivery);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.WorkOrderId = new SelectList(db.WorkOrders, "Id", "IntermediateGoodId", intermediateDelivery.WorkOrderId);
            return View(intermediateDelivery);
        }

    }
}
