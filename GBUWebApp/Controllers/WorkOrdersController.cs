﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GBUWebApp.Models;

namespace GBUWebApp.Controllers
{
    public class WorkOrdersController : Controller
    {
        private gbuEntities db = new gbuEntities();

        // GET: WorkOrders
        public ActionResult Index()
        {
            var workOrders = db.WorkOrders.Include(w => w.IntermediateGood).Include(w => w.Material);
            return View(workOrders.ToList());
        }

        // GET: WorkOrders/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkOrder workOrder = db.WorkOrders.Find(id);
            if (workOrder == null)
            {
                return HttpNotFound();
            }
            return View(workOrder);
        }

        // GET: WorkOrders/Create
        public ActionResult Create()
        {
            ViewBag.IntermediateGoodId = new SelectList(db.IntermediateGoods, "Id", "Name");
            ViewBag.MaterialId = new SelectList(db.Materials, "Id", "Name");
            return View();
        }

        // POST: WorkOrders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IntermediateGoodId,Color,MaterialId,MaterialAmount,Weight,TargetPerShift,EstimationAmount,MachineId,Priority,EstimationStart,EstimationEnd,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] WorkOrder workOrder)
        {
            if (ModelState.IsValid)
            {
                workOrder.Id = "SPK-" + GetCurrentUnixTimestampMillis();
                workOrder.CreateDate = DateTime.Now;
                db.WorkOrders.Add(workOrder);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IntermediateGoodId = new SelectList(db.IntermediateGoods, "Id", "Name", workOrder.IntermediateGoodId);
            ViewBag.MaterialId = new SelectList(db.Materials, "Id", "Name", workOrder.MaterialId);
            return View(workOrder);
        }

        // GET: WorkOrders/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkOrder workOrder = db.WorkOrders.Find(id);
            if (workOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.IntermediateGoodId = new SelectList(db.IntermediateGoods, "Id", "Name", workOrder.IntermediateGoodId);
            ViewBag.MaterialId = new SelectList(db.Materials, "Id", "Name", workOrder.MaterialId);
            return View(workOrder);
        }

        // POST: WorkOrders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IntermediateGoodId,Color,MaterialId,MaterialAmount,Weight,TargetPerShift,EstimationAmount,MachineId,Priority,EstimationStart,EstimationEnd,CreatedBy,CreateDate,UpdatedBy,UpdateDate")] WorkOrder workOrder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(workOrder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IntermediateGoodId = new SelectList(db.IntermediateGoods, "Id", "Name", workOrder.IntermediateGoodId);
            ViewBag.MaterialId = new SelectList(db.Materials, "Id", "Name", workOrder.MaterialId);
            return View(workOrder);
        }

        // GET: WorkOrders/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkOrder workOrder = db.WorkOrders.Find(id);
            if (workOrder == null)
            {
                return HttpNotFound();
            }
            return View(workOrder);
        }

        // POST: WorkOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            WorkOrder workOrder = db.WorkOrders.Find(id);
            db.WorkOrders.Remove(workOrder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public static long GetCurrentUnixTimestampMillis()
        {
            DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime localDateTime, univDateTime;
            localDateTime = DateTime.Now;
            univDateTime = localDateTime.ToUniversalTime();
            return (long)(univDateTime - UnixEpoch).TotalMilliseconds;
        } 

    }
}
