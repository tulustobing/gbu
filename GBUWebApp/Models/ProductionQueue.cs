//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GBUWebApp.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductionQueue
    {
        public int Id { get; set; }
        public string MachineId { get; set; }
        public string WorkOrderId { get; set; }
        public Nullable<int> Priority { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> EstimationStart { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EstimationEnd { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
    
        public virtual Machine Machine { get; set; }
        public virtual WorkOrder WorkOrder { get; set; }
    }
}
