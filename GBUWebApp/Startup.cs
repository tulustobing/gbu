﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GBUWebApp.Startup))]
namespace GBUWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
