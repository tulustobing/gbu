
ALTER TABLE dbo.WorkOrder ADD
	IsDeliveryCreated bit NULL
GO

ALTER TABLE [dbo].[IntermediateDelivery] ALTER COLUMN [Amount] DECIMAL (18) NULL;

GO
ALTER TABLE [dbo].[ProductionDelivery] ALTER COLUMN [Amount] DECIMAL (18) NULL;


