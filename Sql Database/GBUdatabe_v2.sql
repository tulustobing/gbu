CREATE TABLE [dbo].[User] (
    [Id]                NVARCHAR (128) NOT NULL,
    [Email]             NVARCHAR (256) NULL,
    [EmailConfirmed]    BIT            NULL,
    [PasswordHash]      NVARCHAR (MAX) NULL,
    [SecurityStamp]     NVARCHAR (MAX) NULL,
    [TwoFactorEnabled]  BIT            NULL,
    [LockoutEndDateUtc] DATETIME       NULL,
    [LockoutEnabled]    BIT            NULL,
    [AccessFailedCount] INT            NULL,
    [UserName]          NVARCHAR (256) NOT NULL,
    [NamaDepan]         NVARCHAR (256) NULL,
    [NamaBelakang]      NVARCHAR (256) NULL,
    [IdKaryawan]        NVARCHAR (256) NULL,
    [Jabatan]           NVARCHAR (256) NULL,
    CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Roles] (
    [Id]   NVARCHAR (128) NOT NULL,
    [Name] NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.Rolez] PRIMARY KEY CLUSTERED ([Id] ASC)
);


CREATE TABLE [dbo].[UserRoles] (
    [UserId] NVARCHAR (128) NOT NULL,
    [RoleId] NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.UserRolee] PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC)
);


CREATE TABLE [dbo].[UserClaim] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [UserId]     NVARCHAR (128) NOT NULL,
    [ClaimType]  NVARCHAR (MAX) NULL,
    [ClaimValue] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.UserClaim] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.UserClaim_dbo.User_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id]) ON DELETE CASCADE
);
GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[UserClaim]([UserId] ASC);


CREATE TABLE [dbo].[UserLogin] (
    [LoginProvider] NVARCHAR (128) NOT NULL,
    [ProviderKey]   NVARCHAR (128) NOT NULL,
    [UserId]        NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.UserLogin] PRIMARY KEY CLUSTERED ([LoginProvider] ASC, [ProviderKey] ASC, [UserId] ASC),
    CONSTRAINT [FK_dbo.UserLogin_dbo.User_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id]) ON DELETE CASCADE
);
GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[UserLogin]([UserId] ASC);



CREATE TABLE [dbo].[Color]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NULL 
)

CREATE TABLE [dbo].[Material] (
    [Id]     NVARCHAR (50)   NOT NULL,
    [Name]   NVARCHAR (128)  NULL,
    [Color]  NVARCHAR (50)   NULL,
    [Amount] DECIMAL (18, 2) NULL,
    [Price]  DECIMAL (18, 2) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[IntermediateGood] (
    [Id]           NVARCHAR (50)   NOT NULL,
    [Name]         NVARCHAR (128)  NULL,
    [WeightPerPiece]       NVARCHAR (50)   NULL,
    [CyclicTime]   INT             NULL,
    [CreatedBy]    NVARCHAR (50)   NULL,
    [CreateDate]   DATETIME        NULL,
    [UpdatedBy]    NVARCHAR (50)   NULL,
    [UpdateDate]   DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


CREATE TABLE [dbo].[ProductionStock]
(
	[Id]     NVARCHAR (50)   NOT NULL PRIMARY KEY, 
    [MaterialName] NVARCHAR(256) NULL, 
    [Amount] DECIMAL (18, 2) NULL,
);
	

CREATE TABLE [dbo].[Machine] (
    [Id]   NVARCHAR (50)  NOT NULL,
    [Name] NVARCHAR (128) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[AdjusmentFinishedGood] (
    [Id]         INT IDENTITY (1,1)   NOT NULL,
    [Name]       NVARCHAR (50)   NULL,
    [Amount]     DECIMAL (18, 2) NULL,
    [Remarks]    NVARCHAR (MAX)  NULL,
    [CreatedBy]  NVARCHAR (50)   NULL,
    [CreateDate] DATETIME        NULL,
    [UpdatedBy]  NVARCHAR (50)   NULL,
    [UpdateDate] DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


CREATE TABLE [dbo].[AdjusmentIntermediate] (
    [Id]             INT IDENTITY (1,1)   NOT NULL,
    [IntermediateId] NVARCHAR (50)   NULL,
    [Amount]         DECIMAL (18, 2) NULL,
    [Remarks]        NVARCHAR (MAX)  NULL,
    [CreatedBy]      NVARCHAR (50)   NULL,
    [CreateDate]     DATETIME        NULL,
    [UpdatedBy]      NVARCHAR (50)   NULL,
    [UpdateDate]     DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.AdjustIntermendiate] FOREIGN KEY ([IntermediateId]) REFERENCES [dbo].[IntermediateGood] ([Id])
);


CREATE TABLE [dbo].[MaterialPurcasement] (
    [Id]         NVARCHAR (50)   NOT NULL,
    [MaterialId] NVARCHAR (50)   NULL,
    [Amount]     DECIMAL (18, 2) NULL,
    [Price]      DECIMAL (18, 2) NULL,
    [BuyingDate] DATETIME        NULL,
    [Remarks]    NVARCHAR (MAX)  NULL,
    [CreatedBy]  NVARCHAR (50)   NULL,
    [CreateDate] DATETIME        NULL,
    [UpdatedBy]  NVARCHAR (50)   NULL,
    [UpdateDate] DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.BuyingMaterial] FOREIGN KEY ([MaterialId]) REFERENCES [dbo].[Material] ([Id])
);


CREATE TABLE [dbo].[DefectDelivery] (
    [Id]             INT IDENTITY (1,1)   NOT NULL,
    [IntermediateId] NVARCHAR (50)   NULL,
    [Amount]         DECIMAL (18, 2) NULL,
    [Color]          NVARCHAR (50)   NULL,
    [Status]         NVARCHAR (50)   NULL,
    [Remarks]        NVARCHAR (MAX)  NULL,
    [CreatedBy]      NVARCHAR (50)   NULL,
    [CreateDate]     DATETIME        NULL,
    [UpdatedBy]      NVARCHAR (50)   NULL,
    [UpdateDate]     DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Defect] FOREIGN KEY ([IntermediateId]) REFERENCES [dbo].[IntermediateGood] ([Id])
);

CREATE TABLE [dbo].[IntermediateColor] (
    [ColorId] INT  NOT NULL,
    [IntermediateId] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_dbo.Color] PRIMARY KEY CLUSTERED ([IntermediateId] ASC, [ColorId] ASC),
	CONSTRAINT [FK_dbo.ColorIntermendiate] FOREIGN KEY ([IntermediateId]) REFERENCES [dbo].[IntermediateGood] ([Id]),
	CONSTRAINT [FK_dbo.IntermendiateColor] FOREIGN KEY ([ColorId]) REFERENCES [dbo].[Color] ([Id])
);


CREATE TABLE [dbo].[WorkOrder] (
    [Id]                 NVARCHAR (128)  NOT NULL,
    [TotalMaterialAmount]     DECIMAL (18, 2) NULL,
    [WeightPerIntermediate]             DECIMAL (18, 2) NULL,
    [TargetPerShift]     DECIMAL (18, 2) NULL,
    [EstimationAmount]   DECIMAL (18, 2) NULL,
    [MachineId]          NVARCHAR (50)   NULL,
    [Priority]           INT             NULL,
    [EstimationStart]    DATETIME        NULL,
    [EstimationEnd]      DATETIME        NULL,
    [CreatedBy]          NVARCHAR (50)   NULL,
    [CreateDate]         DATETIME        NULL,
    [UpdatedBy]          NVARCHAR (50)   NULL,
    [UpdateDate]         DATETIME        NULL,
    [IsDeliveryCreated]  BIT             NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[WOIntermediate] (
    [Id]                 INT IDENTITY (1,1)  NOT NULL,
    [IntermediateId]          NVARCHAR (50)   NULL,
	[WorkOrderId]   NVARCHAR (128) NULL,
	[IntermediateName]          NVARCHAR (128)   NULL,
    [WeightPerIntermediate]     DECIMAL (18, 2) NULL,
    [Color]     NVARCHAR (50) NULL,
    [EstimationAmount]   DECIMAL (18, 2) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_dbo.IntermendiateWO] FOREIGN KEY ([IntermediateId]) REFERENCES [dbo].[IntermediateGood] ([Id]),
	CONSTRAINT [FK_dbo.IntermendiatsWO] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([Id])
);

CREATE TABLE [dbo].[MaterialWorkList] (
    [Id]                 INT IDENTITY (1,1)  NOT NULL,
    [WorkOrderId]          NVARCHAR (128)   NULL,
	[Priority]          INT  NULL,
    [Status]     NVARCHAR (50) NULL,
	[IsAutoCompleted]     BIT  NULL,
    [Remark]     NVARCHAR (256) NULL,
    [TotalSentMaterial]   DECIMAL (18, 2) NULL,
	[CreatedBy]          NVARCHAR (50)   NULL,
    [CreateDate]         DATETIME        NULL,
    [UpdatedBy]          NVARCHAR (50)   NULL,
    [UpdateDate]         DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_dbo.BBWOList] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([Id])
);

CREATE TABLE [dbo].[WOMaterial] (
    [Id]                 INT IDENTITY (1,1)  NOT NULL,
    [WOIntermediateId]          INT   NULL,
	[WorkOrderId]          NVARCHAR (128)   NULL,
    [MaterialId]     NVARCHAR (50) NULL,
	[MaterialWorkListId] INT NULL,
	[MaterialName]     NVARCHAR (128) NULL,
    [Color]     NVARCHAR (50) NULL,
    [MaterialAmount]   DECIMAL (18, 2) NULL,
	[SentMaterial]   DECIMAL (18, 2) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_dbo.IntermendiateWOBB] FOREIGN KEY ([WOIntermediateId]) REFERENCES [dbo].[WOIntermediate] ([Id]),
	CONSTRAINT [FK_dbo.MaterialWOS] FOREIGN KEY ([MaterialId]) REFERENCES [dbo].[Material] ([Id]),
	CONSTRAINT [FK_dbo.MaterialWL] FOREIGN KEY ([MaterialWorkListId]) REFERENCES [dbo].[MaterialWorkList] ([Id])
);

CREATE TABLE [dbo].[MaterialDelivery] (
    [Id]          NVARCHAR (50)   NOT NULL,
    [MaterialWorkId] INT  NULL,
    [Status]      NVARCHAR (50)   NULL,
    [CreatedBy]   NVARCHAR (50)   NULL,
    [CreateDate]  DATETIME        NULL,
    [UpdatedBy]   NVARCHAR (50)   NULL,
    [UpdateDate]  DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.MaterialWorksss] FOREIGN KEY ([MaterialWorkId]) REFERENCES [dbo].[MaterialWorkList] ([Id])
);


CREATE TABLE [dbo].[MaterialDeliveryDetail] (
    [Id]          INT IDENTITY(1,1)   NOT NULL,
    [MaterialDeliveryId] NVARCHAR (50)   NULL,
    [WorkOrderId]      NVARCHAR (128)   NULL,
	[MaterialId]      NVARCHAR (50)   NULL,
	[SentWeightAmount]   DECIMAL (18, 2) NULL,
	[ReceivedWeightAmount]   DECIMAL (18, 2) NULL,
	[Color]      NVARCHAR (50)   NULL,
	[Remark]     NVARCHAR (256) NULL,
    [CreatedBy]   NVARCHAR (50)   NULL,
    [CreateDate]  DATETIME        NULL,
    [UpdatedBy]   NVARCHAR (50)   NULL,
    [UpdateDate]  DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.MaterialDetailDel] FOREIGN KEY ([MaterialDeliveryId]) REFERENCES [dbo].[MaterialDelivery] ([Id]),
	CONSTRAINT [FK_dbo.MaterialDetailMaterial] FOREIGN KEY ([MaterialId]) REFERENCES [dbo].[Material] ([Id])
);

CREATE TABLE [dbo].[ProductionDelivery] (
    [Id]          NVARCHAR (50)   NOT NULL,
    [Status]      NVARCHAR (50)   NULL,
    [CreatedBy]   NVARCHAR (50)   NULL,
    [CreateDate]  DATETIME        NULL,
    [UpdatedBy]   NVARCHAR (50)   NULL,
    [UpdateDate]  DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
);

CREATE TABLE [dbo].[ProductionDeliveryDetail] (
    [Id]   INT IDENTITY(1,1)   NOT NULL,
    [ProductionDeliveryId] NVARCHAR (50)   NULL,
    [IntermediateId]          NVARCHAR (50)   NULL,
    [Color]      NVARCHAR(50)   NULL,
	[IntermediateName]      NVARCHAR (256)   NULL,
	[SentWeightAmount]   DECIMAL (18, 2) NULL,
	[ReceivedWeightAmount]   DECIMAL (18, 2) NULL,
	[Remark]     NVARCHAR (256) NULL,
    [CreatedBy]   NVARCHAR (50)   NULL,
    [CreateDate]  DATETIME        NULL,
    [UpdatedBy]   NVARCHAR (50)   NULL,
    [UpdateDate]  DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ProdDeDe] FOREIGN KEY ([ProductionDeliveryId]) REFERENCES [dbo].[ProductionDelivery] ([Id]),
	CONSTRAINT [FK_dbo.IntermediateDeIdDe] FOREIGN KEY ([IntermediateId]) REFERENCES [dbo].[IntermediateGood] ([Id])
);

CREATE TABLE [dbo].[IntermediateDelivery] (
    [Id]          NVARCHAR (50)   NOT NULL,
    [Status]      NVARCHAR (50)   NULL,
	[Remark]	  NVARCHAR (50)   NULL,
    [CreatedBy]   NVARCHAR (50)   NULL,
    [CreateDate]  DATETIME        NULL,
    [UpdatedBy]   NVARCHAR (50)   NULL,
    [UpdateDate]  DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
);






CREATE TABLE [dbo].[GoodDetail] (
    [Id]          NVARCHAR (50)   NOT NULL,
	[IntermediateDeliveryId] NVARCHAR (50)  NULL,
    [GoodName]      NVARCHAR (256)   NULL,
	[Amount]	  NVARCHAR (50)   NULL,
	[Unit]	  NVARCHAR (50)   NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_dbo.GoodDel] FOREIGN KEY ([IntermediateDeliveryId]) REFERENCES [dbo].[IntermediateDelivery] ([Id])
);

CREATE TABLE [dbo].[SpecificGoodDetail] (
    [Id]          INT IDENTITY (1,1)   NOT NULL,
	[GoodDetailId] NVARCHAR (50)  NULL,
    [WorkOrderId]      NVARCHAR (50)   NULL,
	[AmountPerColor]	  NVARCHAR (50)   NULL,
	[Color]	  NVARCHAR (50)   NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_dbo.SpecificDetail] FOREIGN KEY ([GoodDetailId]) REFERENCES [dbo].[GoodDetail] ([Id])
);

CREATE TABLE [dbo].[ProductionQueue] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [MachineId]       NVARCHAR (50)  NULL,
    [WorkOrderId]     NVARCHAR (128) NULL,
    [Priority]        INT            NULL,
	[EstimationAmout]   DECIMAL (18, 2) NULL,
	[SentAmout]   DECIMAL (18, 2) NULL,
    [Status]          NVARCHAR (50)  NULL,
    [EstimationStart] DATETIME       NULL,
    [StartDate]       DATETIME       NULL,
	[CreateWODate]       DATETIME       NULL,
    [EstimationEnd]   DATETIME       NULL,
    [EndDate]         DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ProdMachine] FOREIGN KEY ([MachineId]) REFERENCES [dbo].[Machine] ([Id]),
    CONSTRAINT [FK_dbo.ProdWo] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([Id])
);

CREATE TABLE [dbo].[FinishedGood] (
    [Id]     NVARCHAR (50)   NOT NULL,
    [Name]   NVARCHAR (128)  NULL,
    [Unit]  NVARCHAR (50)   NULL,
    [SellingPrice] DECIMAL (18, 2) NULL,
    [DozAmount]  DECIMAL (18, 2) NULL,
	[StockAmount]  DECIMAL (18, 2) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Configuration] (
    [Key]     NVARCHAR (50)   NOT NULL,
    [Value] DECIMAL (18, 2) NULL,
    PRIMARY KEY CLUSTERED ([Key] ASC)
);



