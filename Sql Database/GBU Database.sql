USE [gbu]
GO
/****** Object:  Table [dbo].[AdjusmentFinishedGood]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdjusmentFinishedGood](
	[Id] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Amount] [decimal](18, 2) NULL,
	[Remarks] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AdjusmentIntermediate]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdjusmentIntermediate](
	[Id] [nvarchar](50) NOT NULL,
	[IntermediateId] [nvarchar](50) NULL,
	[Amount] [decimal](18, 2) NULL,
	[Remarks] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DefectDelivery]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DefectDelivery](
	[Id] [nvarchar](50) NOT NULL,
	[IntermediateId] [nvarchar](50) NULL,
	[Amount] [decimal](18, 2) NULL,
	[Color] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[Remarks] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[IntermediateDelivery]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntermediateDelivery](
	[Id] [nvarchar](50) NOT NULL,
	[WorkOrderId] [nvarchar](128) NULL,
	[Amount] [nchar](10) NULL,
	[Status] [nvarchar](50) NULL,
	[Remarks] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[IntermediateGood]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntermediateGood](
	[Id] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](128) NULL,
	[Weight] [nvarchar](50) NULL,
	[Color] [nvarchar](50) NULL,
	[CyclicTime] [int] NULL,
	[SellingPrice] [decimal](18, 2) NULL,
	[Amount] [decimal](18, 2) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Machine]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Machine](
	[Id] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Material]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Material](
	[Id] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](128) NULL,
	[Color] [nvarchar](50) NULL,
	[Amount] [decimal](18, 2) NULL,
	[Price] [numeric](18, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MaterialDelivery]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaterialDelivery](
	[Id] [nvarchar](50) NOT NULL,
	[WorkOrderId] [nvarchar](128) NULL,
	[Amount] [decimal](18, 2) NULL,
	[Status] [nvarchar](50) NULL,
	[Remarks] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MaterialPurcasement]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaterialPurcasement](
	[Id] [nvarchar](50) NOT NULL,
	[MaterialId] [nvarchar](50) NULL,
	[Amount] [decimal](18, 2) NULL,
	[Price] [decimal](18, 2) NULL,
	[BuyingDate] [datetime] NULL,
	[Remarks] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductionDelivery]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductionDelivery](
	[Id] [nvarchar](50) NOT NULL,
	[WorkOrderId] [nvarchar](128) NULL,
	[Amount] [nchar](10) NULL,
	[MachineId] [nvarchar](50) NULL,
	[MachineStatus] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[Remarks] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductionQueue]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductionQueue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MachineId] [nvarchar](50) NULL,
	[WorkOrderId] [nvarchar](128) NULL,
	[Priority] [int] NULL,
	[Status] [nvarchar](50) NULL,
	[EstimationStart] [datetime] NULL,
	[StartDate] [datetime] NULL,
	[EstimationEnd] [datetime] NULL,
	[EndDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [nvarchar](128) NOT NULL,
	[Nama] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[TwoFactorEnabled] [bit] NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NULL,
	[AccessFailedCount] [int] NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[NamaDepan] [nvarchar](256) NULL,
	[NamaBelakang] [nvarchar](256) NULL,
	[IdKaryawan] [nvarchar](256) NULL,
	[Jabatan] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkOrder]    Script Date: 04/02/2017 10:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkOrder](
	[Id] [nvarchar](128) NOT NULL,
	[IntermediateGoodId] [nvarchar](50) NULL,
	[Color] [nvarchar](50) NULL,
	[MaterialId] [nvarchar](50) NULL,
	[MaterialAmount] [decimal](18, 2) NULL,
	[Weight] [decimal](18, 2) NULL,
	[TargetPerShift] [decimal](18, 2) NULL,
	[EstimationAmount] [decimal](18, 2) NULL,
	[MachineId] [nvarchar](50) NULL,
	[Priority] [int] NULL,
	[EstimationStart] [datetime] NULL,
	[EstimationEnd] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[AdjusmentFinishedGood] ([Id], [Name], [Amount], [Remarks], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'ADJSFG-01', N'Cangkir', CAST(40.00 AS Decimal(18, 2)), NULL, N'1105', CAST(N'2017-02-10T02:31:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[AdjusmentIntermediate] ([Id], [IntermediateId], [Amount], [Remarks], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'ADJSI-01', N'IG-1', CAST(40.00 AS Decimal(18, 2)), NULL, N'1104', CAST(N'2017-02-10T02:31:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[DefectDelivery] ([Id], [IntermediateId], [Amount], [Color], [Status], [Remarks], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'DEFDEL-01', N'IG-1', CAST(40.00 AS Decimal(18, 2)), N'Pink', N'ACCEPTED', NULL, N'1103', CAST(N'2017-02-10T02:31:00.000' AS DateTime), N'1102', CAST(N'2017-02-10T02:45:00.000' AS DateTime))
INSERT [dbo].[IntermediateDelivery] ([Id], [WorkOrderId], [Amount], [Status], [Remarks], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'SJDEL-01', N'SPK-001', N'40        ', N'ACCEPTED', NULL, N'1103', CAST(N'2017-02-10T02:31:00.000' AS DateTime), N'1104', CAST(N'2017-02-10T02:45:00.000' AS DateTime))
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-1', N'Body-Cangkir Morie', NULL, NULL, NULL, CAST(14075.00 AS Decimal(18, 2)), CAST(3375.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-10', N'Body - Gant Baju Besar Hitam', NULL, NULL, NULL, CAST(18533.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-11', N'Body - Gant Candy Warna', NULL, NULL, NULL, CAST(18533.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-12', N'Body - Gant Candy Silver', NULL, NULL, NULL, CAST(6254.00 AS Decimal(18, 2)), CAST(11162.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-13', N'Body - Gayung Millenix Warna', NULL, NULL, NULL, CAST(13713.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-14', N'Body - Gayung Millenix Warna Gambar Buah', NULL, NULL, NULL, CAST(18533.00 AS Decimal(18, 2)), CAST(409.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-15', N'Body - Gayung Millenix Silver', NULL, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(148.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-16', N'Body - Kotak Sabun Bening', NULL, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(573.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-17', N'Body - Kotak Sabun DOP', NULL, NULL, NULL, CAST(18533.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-18', N'Body - Kapstock Warna', NULL, NULL, NULL, CAST(11500.00 AS Decimal(18, 2)), CAST(129.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-19', N'Kait - Kapstock Warna', NULL, NULL, NULL, CAST(18533.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-2', N'Tutup- cangkir Tutup Serena Warna / Hijau', NULL, NULL, NULL, CAST(8458.00 AS Decimal(18, 2)), CAST(8926.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-20', N'Body - Kapstock Silver', NULL, NULL, NULL, CAST(12236.00 AS Decimal(18, 2)), CAST(8098.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-21', N'Kait - Kapstock Silver', NULL, NULL, NULL, CAST(14500.00 AS Decimal(18, 2)), CAST(8628.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-22', N'Body - Mangkok 101 W/P', NULL, NULL, NULL, CAST(6000.00 AS Decimal(18, 2)), CAST(1197.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-23', N'Body - Mangkok 105 W/P/Hijau (PP)', NULL, NULL, NULL, CAST(6000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-24', N'Body - Mangkok 105 W/P/Hijau (PS / Melamix)', NULL, NULL, NULL, CAST(6000.00 AS Decimal(18, 2)), CAST(1197.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-25', N'Body - Mangkok 7 Melamix', NULL, NULL, NULL, CAST(6000.00 AS Decimal(18, 2)), CAST(498.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-26', N'Body - Mangkok 7 DOP', NULL, NULL, NULL, CAST(15842.00 AS Decimal(18, 2)), CAST(31925.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-27', N'Body - Mangkok Sambal / Cuka Warna', NULL, NULL, NULL, CAST(18000.00 AS Decimal(18, 2)), CAST(25.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-28', N'Body - Nampan Fortuna Gambar', NULL, NULL, NULL, CAST(12000.00 AS Decimal(18, 2)), CAST(6235.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-29', N'Body - Piring 8" New DOP', NULL, NULL, NULL, CAST(15000.00 AS Decimal(18, 2)), CAST(308.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-3', N'Body- Ember 1.5 Galon Silver GG Kawat', NULL, NULL, NULL, CAST(12189.00 AS Decimal(18, 2)), CAST(3404.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-30', N'Body - Piring 9" Tulip', NULL, NULL, NULL, CAST(13000.00 AS Decimal(18, 2)), CAST(1275.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-4', N'Gagang- Ember 1.5 Galon Silver GG Kawat', NULL, NULL, NULL, CAST(14750.00 AS Decimal(18, 2)), CAST(2100.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-5', N'Body - Ember 1.5 Galon Silver + TTP GG Kawat', NULL, NULL, NULL, CAST(19750.00 AS Decimal(18, 2)), CAST(12500.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-6', N'Gagang - Ember 1.5 Galon Silver + TTP GG Kawat', NULL, NULL, NULL, CAST(18533.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-7', N'Tutup - Ember 1.5 Galon Silver + TTP GG Kawat', NULL, NULL, NULL, CAST(18533.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-8', N'Body - Gant Baju Bening GBU', NULL, NULL, NULL, CAST(18533.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[IntermediateGood] ([Id], [Name], [Weight], [Color], [CyclicTime], [SellingPrice], [Amount], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'IG-9', N'Body - Gant Baju Besar Warna', NULL, NULL, NULL, CAST(8300.00 AS Decimal(18, 2)), CAST(1564.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[Machine] ([Id], [Name]) VALUES (N'MCHN-1', N'Machine 1')
INSERT [dbo].[Machine] ([Id], [Name]) VALUES (N'MCHN-2', N'Machine 2')
INSERT [dbo].[Machine] ([Id], [Name]) VALUES (N'MCHN-3', N'Machine 3')
INSERT [dbo].[Machine] ([Id], [Name]) VALUES (N'MCHN-4', N'Machine 4')
INSERT [dbo].[Machine] ([Id], [Name]) VALUES (N'MCHN-5', N'Machine 5')
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-1', N'PP ORI Masplene / Titanpro', NULL, CAST(3375.00 AS Decimal(18, 2)), CAST(14075.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-10', N'PS Gil Hitam / DOP', NULL, CAST(10.00 AS Decimal(18, 2)), CAST(18533.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-11', N'LDPE Petlin', NULL, CAST(10.00 AS Decimal(18, 2)), CAST(18533.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-12', N'PS Gilingan Yellowish Ajang', NULL, CAST(11162.00 AS Decimal(18, 2)), CAST(6254.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-13', N'PS Gilingan Lengkeng', NULL, CAST(1.00 AS Decimal(18, 2)), CAST(13713.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-14', N'OPP', NULL, CAST(409.00 AS Decimal(18, 2)), CAST(18533.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-15', N'BS-PS', NULL, CAST(148.00 AS Decimal(18, 2)), CAST(0.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-16', N'BS-PP', NULL, CAST(573.00 AS Decimal(18, 2)), CAST(0.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-17', N'PP PK (B)', NULL, CAST(10.00 AS Decimal(18, 2)), CAST(18533.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-18', N'PP PK (C)', NULL, CAST(129.00 AS Decimal(18, 2)), CAST(11500.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-19', N'PP Elpro', NULL, CAST(10.00 AS Decimal(18, 2)), CAST(18533.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-2', N'PP KK', NULL, CAST(8926.00 AS Decimal(18, 2)), CAST(8458.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-20', N'PK Proses', NULL, CAST(8098.00 AS Decimal(18, 2)), CAST(12236.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-21', N'PS Gilingan AL', NULL, CAST(8628.00 AS Decimal(18, 2)), CAST(14500.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-22', N'PS Dop', NULL, CAST(1197.00 AS Decimal(18, 2)), CAST(6000.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-23', N'PS Gilingan Warna Biru', NULL, CAST(2000.00 AS Decimal(18, 2)), CAST(6000.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-24', N'PS Gilingan Warna Hijau', NULL, CAST(1197.00 AS Decimal(18, 2)), CAST(6000.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-25', N'PS Gilingan Warna Merah', NULL, CAST(498.00 AS Decimal(18, 2)), CAST(6000.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-26', N'PP Ori Petrovietnam', NULL, CAST(31925.00 AS Decimal(18, 2)), CAST(15842.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-27', N'LDPE B', NULL, CAST(25.00 AS Decimal(18, 2)), CAST(18000.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-28', N'PP Silver (MP)', NULL, CAST(6235.00 AS Decimal(18, 2)), CAST(12000.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-29', N'PS Gilingan (SP)', NULL, CAST(308.00 AS Decimal(18, 2)), CAST(15000.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-3', N'PP Silver', NULL, CAST(3404.00 AS Decimal(18, 2)), CAST(12189.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-30', N'PP Super', NULL, CAST(1275.00 AS Decimal(18, 2)), CAST(13000.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-4', N'Ps Gilingan', NULL, CAST(2100.00 AS Decimal(18, 2)), CAST(14750.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-5', N'PS ORI TRINSEO', NULL, CAST(12500.00 AS Decimal(18, 2)), CAST(19750.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-6', N'PS Gilingan Hengky', NULL, CAST(70.00 AS Decimal(18, 2)), CAST(18533.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-7', N'PS Gilingan Gabby', NULL, CAST(10.00 AS Decimal(18, 2)), CAST(18533.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-8', N'PS Gilingan BUDI', NULL, CAST(10.00 AS Decimal(18, 2)), CAST(18533.00 AS Numeric(18, 2)))
INSERT [dbo].[Material] ([Id], [Name], [Color], [Amount], [Price]) VALUES (N'BB-9', N'PS Gil Warna', NULL, CAST(1564.00 AS Decimal(18, 2)), CAST(8300.00 AS Numeric(18, 2)))
INSERT [dbo].[MaterialDelivery] ([Id], [WorkOrderId], [Amount], [Status], [Remarks], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'BBDEL-01', N'SPK-001', CAST(800.00 AS Decimal(18, 2)), N'ACCEPTED', NULL, N'1102', CAST(N'2017-02-10T02:00:00.000' AS DateTime), N'1103', CAST(N'2017-02-10T02:30:00.000' AS DateTime))
INSERT [dbo].[MaterialPurcasement] ([Id], [MaterialId], [Amount], [Price], [BuyingDate], [Remarks], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'PURC-01', N'BB-1', CAST(20.00 AS Decimal(18, 2)), CAST(4500.00 AS Decimal(18, 2)), CAST(N'2017-02-10T01:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ProductionDelivery] ([Id], [WorkOrderId], [Amount], [MachineId], [MachineStatus], [Status], [Remarks], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'PRODDEL-01', N'SPK-001', N'50        ', N'MCHN-1', N'DONE', N'ACCEPTED', NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ProductionQueue] ON 

INSERT [dbo].[ProductionQueue] ([Id], [MachineId], [WorkOrderId], [Priority], [Status], [EstimationStart], [StartDate], [EstimationEnd], [EndDate]) VALUES (1, N'MCHN-1', N'SPK-001', 1, N'PENDING', CAST(N'2017-02-10T01:00:00.000' AS DateTime), CAST(N'2017-02-10T02:00:00.000' AS DateTime), CAST(N'2017-02-10T02:00:00.000' AS DateTime), CAST(N'2017-02-10T03:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[ProductionQueue] OFF
INSERT [dbo].[Role] ([Id], [Nama]) VALUES (N'1', N'SuperAdmin')
INSERT [dbo].[Role] ([Id], [Nama]) VALUES (N'2', N'BBAdmin')
INSERT [dbo].[Role] ([Id], [Nama]) VALUES (N'3', N'ProdAdmin')
INSERT [dbo].[Role] ([Id], [Nama]) VALUES (N'4', N'ProdSjAdmin')
INSERT [dbo].[Role] ([Id], [Nama]) VALUES (N'5', N'ProdJdAdmin')
INSERT [dbo].[User] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [NamaDepan], [NamaBelakang], [IdKaryawan], [Jabatan]) VALUES (N'1101', N'leader@leader.com', NULL, N'leader123', NULL, NULL, NULL, NULL, NULL, N'adminspk', N'Jaka', N'Sembung', N'11112055', N'Direktur')
INSERT [dbo].[User] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [NamaDepan], [NamaBelakang], [IdKaryawan], [Jabatan]) VALUES (N'1102', N'budi@bb.com', NULL, N'bb123', NULL, NULL, NULL, NULL, NULL, N'bbadmin', N'Budi', N'Hermansyah', N'11112050', N'Manager BB')
INSERT [dbo].[User] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [NamaDepan], [NamaBelakang], [IdKaryawan], [Jabatan]) VALUES (N'1103', N'tri@prod.com', NULL, N'prod123', NULL, NULL, NULL, NULL, NULL, N'prodadmin', N'Tri', N'Pujianto', N'11112045', N'Manager Prod')
INSERT [dbo].[User] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [NamaDepan], [NamaBelakang], [IdKaryawan], [Jabatan]) VALUES (N'1104', N'agung@sj.com', NULL, N'sj123', NULL, NULL, NULL, NULL, NULL, N'sjadmin', N'Agung', N'Jumandi', N'11112040', N'Manager Prod Setengah Jadi')
INSERT [dbo].[User] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [NamaDepan], [NamaBelakang], [IdKaryawan], [Jabatan]) VALUES (N'1105', N'slamet@prodjd.com', NULL, N'prodjd123', NULL, NULL, NULL, NULL, NULL, N'pjadmin', N'Slamet', N'Cahyono', N'11112035', N'Manager Prod Jadi')
INSERT [dbo].[UserRole] ([UserId], [RoleId]) VALUES (N'1101', N'1')
INSERT [dbo].[UserRole] ([UserId], [RoleId]) VALUES (N'1102', N'2')
INSERT [dbo].[UserRole] ([UserId], [RoleId]) VALUES (N'1103', N'3')
INSERT [dbo].[UserRole] ([UserId], [RoleId]) VALUES (N'1104', N'4')
INSERT [dbo].[UserRole] ([UserId], [RoleId]) VALUES (N'1105', N'5')
INSERT [dbo].[WorkOrder] ([Id], [IntermediateGoodId], [Color], [MaterialId], [MaterialAmount], [Weight], [TargetPerShift], [EstimationAmount], [MachineId], [Priority], [EstimationStart], [EstimationEnd], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate]) VALUES (N'SPK-001', N'IG-1', N'Pink', N'BB-1', CAST(800.00 AS Decimal(18, 2)), CAST(146.00 AS Decimal(18, 2)), CAST(1.08 AS Decimal(18, 2)), CAST(5470.00 AS Decimal(18, 2)), N'MCHN-1', 1, NULL, NULL, NULL, NULL, NULL, NULL)
ALTER TABLE [dbo].[AdjusmentIntermediate]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AdjustIntermendiate] FOREIGN KEY([IntermediateId])
REFERENCES [dbo].[IntermediateGood] ([Id])
GO
ALTER TABLE [dbo].[AdjusmentIntermediate] CHECK CONSTRAINT [FK_dbo.AdjustIntermendiate]
GO
ALTER TABLE [dbo].[DefectDelivery]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Defect] FOREIGN KEY([IntermediateId])
REFERENCES [dbo].[IntermediateGood] ([Id])
GO
ALTER TABLE [dbo].[DefectDelivery] CHECK CONSTRAINT [FK_dbo.Defect]
GO
ALTER TABLE [dbo].[IntermediateDelivery]  WITH CHECK ADD  CONSTRAINT [FK_dbo.InterWo] FOREIGN KEY([WorkOrderId])
REFERENCES [dbo].[WorkOrder] ([Id])
GO
ALTER TABLE [dbo].[IntermediateDelivery] CHECK CONSTRAINT [FK_dbo.InterWo]
GO
ALTER TABLE [dbo].[MaterialDelivery]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MaterialWO] FOREIGN KEY([WorkOrderId])
REFERENCES [dbo].[WorkOrder] ([Id])
GO
ALTER TABLE [dbo].[MaterialDelivery] CHECK CONSTRAINT [FK_dbo.MaterialWO]
GO
ALTER TABLE [dbo].[MaterialPurcasement]  WITH CHECK ADD  CONSTRAINT [FK_dbo.BuyingMaterial] FOREIGN KEY([MaterialId])
REFERENCES [dbo].[Material] ([Id])
GO
ALTER TABLE [dbo].[MaterialPurcasement] CHECK CONSTRAINT [FK_dbo.BuyingMaterial]
GO
ALTER TABLE [dbo].[ProductionDelivery]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MachineProd] FOREIGN KEY([MachineId])
REFERENCES [dbo].[Machine] ([Id])
GO
ALTER TABLE [dbo].[ProductionDelivery] CHECK CONSTRAINT [FK_dbo.MachineProd]
GO
ALTER TABLE [dbo].[ProductionDelivery]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProdsWo] FOREIGN KEY([WorkOrderId])
REFERENCES [dbo].[WorkOrder] ([Id])
GO
ALTER TABLE [dbo].[ProductionDelivery] CHECK CONSTRAINT [FK_dbo.ProdsWo]
GO
ALTER TABLE [dbo].[ProductionQueue]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProdMachine] FOREIGN KEY([MachineId])
REFERENCES [dbo].[Machine] ([Id])
GO
ALTER TABLE [dbo].[ProductionQueue] CHECK CONSTRAINT [FK_dbo.ProdMachine]
GO
ALTER TABLE [dbo].[ProductionQueue]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProdWo] FOREIGN KEY([WorkOrderId])
REFERENCES [dbo].[WorkOrder] ([Id])
GO
ALTER TABLE [dbo].[ProductionQueue] CHECK CONSTRAINT [FK_dbo.ProdWo]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_dbo.Roles]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_dbo.Users]
GO
ALTER TABLE [dbo].[WorkOrder]  WITH CHECK ADD  CONSTRAINT [FK_dbo.WoGood] FOREIGN KEY([IntermediateGoodId])
REFERENCES [dbo].[IntermediateGood] ([Id])
GO
ALTER TABLE [dbo].[WorkOrder] CHECK CONSTRAINT [FK_dbo.WoGood]
GO
ALTER TABLE [dbo].[WorkOrder]  WITH CHECK ADD  CONSTRAINT [FK_dbo.WoMaterial] FOREIGN KEY([MaterialId])
REFERENCES [dbo].[Material] ([Id])
GO
ALTER TABLE [dbo].[WorkOrder] CHECK CONSTRAINT [FK_dbo.WoMaterial]
GO
USE [master]
GO
ALTER DATABASE [gbu] SET  READ_WRITE 
GO
